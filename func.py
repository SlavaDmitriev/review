from MainWindow import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QLabel, QDialog, QLineEdit, QRadioButton,QGridLayout
from docx import Document
from docx.shared import Inches
from docx.text.run import Font, Run
from docx.shared import RGBColor, Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH
import os, os.path
import comtypes.client
import pypandoc
from pdf2image import convert_from_path
from win32com import client

from pdf2image import convert_from_path

from wand import image as wi

class Window(Ui_MainWindow):
    def __init__(self):
        super(Window, self).__init__()


    def UserUI(self, MainWindow):


        """===================================================================================================================="""


        self.ElementsGrid = QtWidgets.QVBoxLayout()
        self.TitleGroup = QtWidgets.QGroupBox()
        self.ElementsGrid.addStretch()
        self.Elements = []


        self.actionHeader_1.triggered.connect(lambda: self.Header_1(len(self.Elements)))


        self.TitleGroup.setLayout(self.ElementsGrid)
        self.scrollArea_2.setWidget(self.TitleGroup)


        self.actionTitle_page.triggered.connect(self.wind)
        self.pushButton.clicked.connect(self.push)


    def Header_1(self, Position):

        self.header = QtWidgets.QGroupBox('header 1')
        self.header.setFixedSize(850,100)


        self.layout = QtWidgets.QGridLayout()


        self.HeaderText = QtWidgets.QLineEdit()
        self.HeaderText.setText(str(Position))


        self.OkButton = QtWidgets.QPushButton('OK')


        self.DeleteButton = QtWidgets.QPushButton('Delete')


        self.layout.addWidget(self.HeaderText, 0, 0)
        self.layout.addWidget(self.OkButton, 0, 2)
        self.layout.addWidget(self.DeleteButton, 0, 3)

        self.header.setLayout(self.layout)

        self.Elements.append(self.header)
        
        self.ElementsGrid.addWidget(self.header, Position)
        # self.ElementsGrid.addStretch()
        # self.header.addStretch()

        self.OkButton.clicked.connect(self.ok1)
        # self.DeleteButton.clicked.connect(self.delete1)


    def ok1(self):


        document = Document()
        
        run = document.add_heading(0).add_run(self.HeaderText.text())


        font = run.font
        font.name = "Times New Romans"

        font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        document.save('func.docx')


        # document.add_heading(self.HeaderText.text(), 1)

        # # font.color.rgb = RGBColor(0xff, 0xff, 0xff)
        # # document.save('func.docx')
        # # a = self.HeaderText.text()
        # # print(a)
        # # return a




        self.ElementsGrid = QtWidgets.QGridLayout()
        self.TitleGroup = QtWidgets.QGroupBox()




        self.TitleGroup.setLayout(self.ElementsGrid)
        self.scrollArea.setWidget(self.TitleGroup)



        self.header = QtWidgets.QGroupBox()

        self.layout = QtWidgets.QGridLayout()

        self.ht = QLabel(self.HeaderText.text())
        self.ht.resize(30,30)
        self.layout.addWidget(self.ht)


        self.header.setLayout(self.layout)

        
        self.ElementsGrid.addWidget(self.header)



        # document = Document()
        # paragraph = document.add_heading(self.HeaderText.text(), 0)
        # paragraph_format = paragraph.paragraph_format
        #paragraph_format.left_indent
        # paragraph_format.left_indent = Inches(0.49)
        #paragraph_format.left_indent
        #paragraph_format.left_indent.inches
        # # # padagraph = document.font
        # font = paragraph_format.font
        # font.name = "Times New Romans"
        # font.color.rgb = RGBColor(0x00, 0x00, 0x00)
        
        # paragraph = document
        # paragraph_format = paragraph.paragraph_format
        # paragraph_format.left_indent
        # paragraph_format.left_indent = Inches(0.49)
        # paragraph_format.left_indent
        # paragraph_format.left_indent.inches
        # document.save('func.docx')

        # document = Document()
        # paragraph = document.add_heading(self.HeaderText.text(), 0)
        # paragraph_format = paragraph.paragraph_format
        # paragraph_format.left_indent
        # paragraph_format.left_indent = Inches(0.49)
        # paragraph_format.left_indent
        # paragraph_format.left_indent.inches
        # run = padagraph.add_run()
        # font = run.font
        # font.name = "Times New Romans"
        # font.color.rgb = RGBColor(0x00, 0x00, 0x00)

        # document.save('func.docx')

















        folder = "C:\\project"
        file_type = 'docx'
        out_folder = folder

        os.chdir(folder)

        if not os.path.exists(out_folder):
            os.makedirs(out_folder)

        try:
            for files in os.listdir("."):
                if files.endswith(".docx"):
                    out_name = files.replace(file_type, r"pdf")
                    in_file = os.path.abspath(folder + "\\" + files)
                    out_file = os.path.abspath(out_folder + "\\" + out_name)
                    word = client.Dispatch("Word.Application")
                    doc = word.Documents.Open(in_file)
                    doc.SaveAs(out_file, FileFormat=17)
                    doc.Close()
                    word.Quit()
        except:
            print("error")


        # filename = 'func.pdf'
         
        # i = 1

        # page = convert_from_path(filename, last_page=i+1, first_page=i)[0]
 
        # base_filename = os.path.splitext(os.path.basename(filename))[0] + f'_0{i}.jpg'
 
        # save_dir = 'output'
 
        # page.save(os.path.join(save_dir, base_filename), 'JPEG')
 
        # i += 1







        # pdf = wi(filename="func.pdf", resolution=300)
        # pdfImage = pdf.convert("jpeg")
        # i = 1
        # for img in pdfImage.sequence:
        #     page = wi(image=img)
        #     page.save(filename=str(i)+".jpg")
        #     i += 1


        #pages = convert_from_path(r'С\\project\\func.pdf', 500)
        #for page in pages:
            #page.save('out.jpg', 'JPEG')






    # def delete1(self):
    #     self.Elements.pop()




    #     self.ElementsGrid = QtWidgets.QGridLayout()
    #     self.TitleGroup = QtWidgets.QGroupBox()
    #     self.Elements = []


    #     self.actionHeader_1.triggered.connect(self.addh1)

    # def addh1(self):
    #     self.header =QtWidgets.QGroupBox('header 1')
    #     self.header.setFixedSize(850,100)
    #     self.layout = QtWidgets.QGridLayout()
    #     self.HeaderText = QtWidgets.QLineEdit()
    #     self.OkButton = QtWidgets.QPushButton('OK')
    #     self.DeleteButton = QtWidgets.QPushButton('Delete')
    #     self.layout.addWidget(self.HeaderText, 0, 0)
    #     self.layout.addWidget(self.OkButton, 0, 2)
    #     self.layout.addWidget(self.DeleteButton, 0, 3)
    #     self.header.setLayout(self.layout)

    #     # self.Elements[i].append(self.header)

    #     self.OkButton.clicked.connect(self.ok1)

    # def ok1(self):
    #     pass











        """===================================================================================================================="""

    def push(self):
        self.w6 = Window4()
        self.w6.show()


    def wind(self):
        self.w1 = Window1()
        if self.w1.exec() == 1:
            print(w1.getResult())


class Window1(QDialog):
    def __init__(self):
        super(Window1, self).__init__()
        self.setWindowTitle('Window1')
        self.setFixedSize(400,200)

        layoutzzz = QGridLayout(self)
        self.setLayout(layoutzzz)

        self.button1 = QPushButton(self)
        self.button1.setText("Создать новый файл")
        layoutzzz.addWidget(self.button1, 1, 0)
        self.button1.clicked.connect(self.next)


    def next(self):
        self.next = Window2()
        self.next.show()
        # self.next.getResult()


class Window2(QDialog):
    

    def __init__(self):
        super(Window2, self).__init__()

        self.setWindowTitle('Window2')
        self.setFixedSize(400,200)


        layoutrrr = QGridLayout(self)
        self.setLayout(layoutrrr)


        self.lab = QLabel(self)
        self.lab.setText("Название документа")
        layoutrrr.addWidget(self.lab, 0, 0)


        self.titleEdit = QLineEdit(self)
        layoutrrr.addWidget(self.titleEdit, 1, 0)


        self.radio1 = QRadioButton("Лабораторная работа")
        layoutrrr.addWidget(self.radio1, 2, 0)
        self.radio2 = QRadioButton("Практическая работа")
        layoutrrr.addWidget(self.radio2, 3, 0)


        self.radio3 = QRadioButton("Дипломная работа")
        layoutrrr.addWidget(self.radio3, 4, 0)


        self.radio4 = QRadioButton("Курсовая работа")
        layoutrrr.addWidget(self.radio4, 5, 0)


        self.btnrrr = QPushButton(self) 
        self.btnrrr.setText("Продолжить")
        self.btnrrr.clicked.connect(self.contin)
        layoutrrr.addWidget(self.btnrrr, 5, 1)

        self.btnrrr1 = QPushButton(self) 
        self.btnrrr1.setText("Отмена")
        self.btnrrr1.clicked.connect(self.reject)
        layoutrrr.addWidget(self.btnrrr, 5, 2)


    def getResult(self):
        return self.titleEdit.text()


    def contin(self):
        self.contin = Window3()
        #self.contin.show()
        self.contin.exec()
        
        # self.contin.getResult()
        



class Window3(QDialog):
    def __init__(self):
        super(Window3, self).__init__()
        self.setWindowTitle('Window3')
        self.setFixedSize(400,200)


        layoutbbb = QGridLayout(self)
        self.setLayout(layoutbbb)


        self.labe = QLabel(self)
        self.labe.setText("Ваше имя")
        layoutbbb.addWidget(self.labe, 0, 0)


        self.titleEditbbb1 = QLineEdit(self)
        layoutbbb.addWidget(self.titleEditbbb1, 1, 0) 


        self.labe1 = QLabel(self)
        self.labe1.setText("Ваша группа")
        layoutbbb.addWidget(self.labe1, 2, 0)


        self.titleEditbbb2 = QLineEdit(self)
        layoutbbb.addWidget(self.titleEditbbb2, 3, 0) 


        self.labe2 = QLabel(self)
        self.labe2.setText("Имя преподавателя")
        layoutbbb.addWidget(self.labe2, 4, 0)


        self.titleEditbbb3 = QLineEdit(self)
        layoutbbb.addWidget(self.titleEditbbb3, 5, 0) 


        self.btnbbb = QPushButton(self)
        self.btnbbb.setText("Продолжить")
        self.btnbbb.clicked.connect(self.mainbbb)
        layoutbbb.addWidget(self.btnbbb, 5, 1) 


    def mainbbb(self):
       pass

    # def getResult(self):
    #     return self.titleEditbbb1.text()

       
class Window4(QDialog):
    def __init__(self):
        super(Window4, self).__init__()
        self.setWindowTitle('Window4')
        self.setFixedSize(400,100)


        layoutqqq = QGridLayout(self)
        self.setLayout(layoutqqq)


        self.labqqq = QLabel(self)
        self.labqqq.setText("Сохранить как")
        layoutqqq.addWidget(self.labqqq, 0, 0)


        self.radioqqq1 = QRadioButton("docx")
        layoutqqq.addWidget(self.radioqqq1, 1, 0)


        self.radioqqq2 = QRadioButton("png")
        layoutqqq.addWidget(self.radioqqq2, 1, 1)


        self.btnqqq = QPushButton(self)
        self.btnqqq.setText("Продолжить")
        layoutqqq.addWidget(self.btnqqq,1,2)


        self.btnqqq.clicked.connect(self.chois)


    def chois(self):
        self.w7 = Window5()
        self.w7.show()


class Window5(QDialog):


    def __init__(self):
        super(Window5, self).__init__()
        self.setWindowTitle('Window5')
        self.setFixedSize(400,100)


        layoutzzz = QGridLayout(self)
        self.setLayout(layoutzzz)


        self.labzzz = QLabel(self)
        self.labzzz.setText("Окно выбора места сохранения")
        layoutzzz.addWidget(self.labzzz, 0, 0)


        self.btnzzz = QPushButton(self)
        self.btnzzz.setText("Выбор")
        layoutzzz.addWidget(self.btnzzz, 1, 0)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Window()
    ui.setupUi(MainWindow)
    ui.UserUI(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
